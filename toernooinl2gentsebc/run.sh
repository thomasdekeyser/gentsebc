#! /bin/sh

java -Xmx250m -jar webharvest_all_2.jar config=scripts/fetchOV.xml loglevel=error workdir=.
java -Xmx250m -jar webharvest_all_2.jar config=scripts/fetchRanking.xml loglevel=error workdir=.
java -Xmx250m -jar webharvest_all_2.jar config=scripts/fetchCompetitionResults.xml loglevel=error workdir=.
